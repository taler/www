# This file is in the public domain.

# Hardly anyone seems to read README files anymore, so keep this note here:
# Don't remove the variables for python etc. They exist
# because one system sticks with PEPs, and others opt
# for installing every version side-by-side,
# Same goes for babel.

include config.mk

# All: build HTML pages in all languages and compile the
# TypeScript logic in web-common.
all: locale template
	cd web-common && $(TSC)

# Extract translateable strings from jinja2 templates.
locale/messages.pot: *.j2 common/*.j2 common/*.j2.inc
	env PYTHONPATH="." $(BABEL) extract -F locale/babel.map -o locale/messages.pot .

# Update translation (.po) files with new strings.
locale-update: locale/messages.pot
	msgmerge -U -m --previous locale/en/LC_MESSAGES/messages.po locale/messages.pot 
	msgmerge -U -m --previous locale/de/LC_MESSAGES/messages.po locale/messages.pot
	msgmerge -U -m --previous locale/fr/LC_MESSAGES/messages.po locale/messages.pot
	msgmerge -U -m --previous locale/es/LC_MESSAGES/messages.po locale/messages.pot
	msgmerge -U -m --previous locale/it/LC_MESSAGES/messages.po locale/messages.pot
	msgmerge -U -m --previous locale/ru/LC_MESSAGES/messages.po locale/messages.pot
	msgmerge -U -m --previous locale/pt/LC_MESSAGES/messages.po locale/messages.pot

	if grep -nA1 '#-#-#-#-#' locale/*/LC_MESSAGES/messages.po; then echo -e "\nERROR: Conflicts encountered in PO files.\n"; exit 1; fi

# Compile translation files for use.
locale-compile:
	$(BABEL) compile -d locale -l en --use-fuzzy
	$(BABEL) compile -d locale -l de --use-fuzzy
	$(BABEL) compile -d locale -l fr --use-fuzzy
	$(BABEL) compile -d locale -l it --use-fuzzy
	$(BABEL) compile -d locale -l es --use-fuzzy
	$(BABEL) compile -d locale -l ru --use-fuzzy
	$(BABEL) compile -d locale -l pt --use-fuzzy

# Process everything related to gettext translations.
locale: locale-update locale-compile

# Run the jinja2 templating engine to expand templates to HTML
# incorporating translations.
template: locale-compile
	$(PYTHON) ./template.py

run: all
	@[ "$(BROWSER)" ] || ( echo "You need to export the environment variable 'BROWSER' to run this."; exit 1 )
	$(RUN_BROWSER) http://0.0.0.0:8000 &
	# cd rendered && $(PYTHON) -m http.server
	$(PYTHON) -m http.server

clean:
	rm -rf __pycache__
	rm -rf en/ de/ fr/ it/ es/ ru/
	rm -rf rendered/

submodules/init:
	git submodule update --init --recursive

submodules/update:
	git submodule update --recursive --remote

.SILENT: show-help

show-help:
	printf "all:\t\t\tBuild the website\n"
	printf "locale/messages.pot:\tExtract translateable strings from jinja2 templates.\n"
	printf "locale-update:\t\tUpdate translation files with new strings.\n"
	printf "locale-compile:\t\tCompile translation files for use.\n"
	printf "locale:\t\t\tProcess everything related to gettext translations.\n"
	printf "template:\t\texpand jinja2 templates to html.\n"
	printf "run:\t\t\tspawn python webserver and open the current directory.\n"
	printf "clean:\t\t\tclean.\n"
	printf "submodules/init:\tinit git submodules\n"
	printf "submodules/update:\tupdate git submodules\n"

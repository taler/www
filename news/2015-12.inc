          <h3>2015-12: Taler Demo for Chrome/Chromium online</h3>
          <p>We finally have a first simple demo for Taler online. The Firefox-variant
             still needs some love, but you can start to try out the demo using
             the Chrome/Chromium browser at <a href="https://demo.taler.net/">demo.taler.net</a>.
             Please provide feedback to our <a href="https://gnunet.org/bugs/">bug tracker</a>.</p>
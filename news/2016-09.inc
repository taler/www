          <h3>2016-09: Taler Web Payments paper published</h3>
          <p>We just finished the camera-ready version of our paper on how to use Taler
             for Web payments.  This paper does not discuss the cryptography behind
             Taler, but focuses on the practical aspects of how the wallet and the merchants
             interact over the Web.  Hence, this paper should be a good read for
             anyone who wants to integrate Taler support with their Web site.
            We have posted the paper <a href="https://gnunet.org/taler2016space">here</a>.</p>
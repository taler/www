          <h3>2016-06: GNU Taler 0.0.0 released</h3>
          <p>We have reached our first big milestone, the 0.0.0 release!
             The release includes implementations of a bank, exchange, merchant and wallet
             and is available on the GNU FTP mirrors.
             <br>
             While the exchange implements the
             complete protocol, the implementations of merchant and wallet are both
             fundamentally incomplete and still lack key features, including important
             error handling. GNU Taler still lacks an implementation of an auditor or
             logic for integration with "real" banks.
             Thus, this release should not yet be used for actual financial
             transactions.
             <br>
             That said, you can already setup your own functional payment system
             and run your own toy currency -- or just try out the demo using
             the Chrome/Chromium browser at <a href="https://demo.taler.net/">demo.taler.net</a>.
             <br>
             Please provide feedback to our <a href="https://gnunet.org/bugs/">bug tracker</a>.
             There, you can also find our <a href="https://gnunet.org/bugs/roadmap_page.php">roadmap</a>
             which contains a list of known open issues and our plans for the near future.
          </p>

          <h3>2017-10: GNU Taler v0.4.0 released</h3>
          <p>We are happy to announce the release GNU Taler v0.4.0 with support for customer
	     refunds, protocol versioning, incremental key material download, returning funds
	     from the wallet directly back into one's bank account,
	     and various other minor improvements. The Chrome and Chromium wallets
	     are available for download via the App store.  The exchange, merchant
	     backend and bank components are on the GNU FTP mirrors.  Note that the
	     Firefox wallet will take a few more days to become available in the
	     App store due to the Mozilla review process.</p>

          <h3>2017-01: Taler Documentation pages updated</h3>
            <p><a href="https://docs.taler.net/">docs.taler.net</a> is now
               online, providing a new portal to the Taler system documentation.</p>

          <h3>2014-12: Watch Christian Grothoff's FOSSA talk on Taler</h3>
          <p>
           <video id="video" poster="/images/fossa2014.png" autobuffer="" height="360" width="640" controls="controls">
           <source src="/videos/grothoff2014fossa.webm" type="video/webm">
           <source src="/videos/grothoff2014fossa.ogv" type="video/ogv">
           </video>
          </p>
          <p>
          <a rel="license" href="https://creativecommons.org/licenses/by-nd/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="{{ url('/images/ccby.png')}}"></a><br>"<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Taler</span>" by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Christian Grothoff, produced by FOSSA, Inria Rennes Bretagne Atlantique</span> is licensed under a <a rel="license" href="https://creativecommons.org/licenses/by-nd/3.0/deed.en_US">Creative Commons Attribution NoDerivatives 3.0 Unported License</a>.
          </p>

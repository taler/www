          <h3>2016-10: Taler Wallet for Firefox online</h3>
          <p>We now have a first version of the Taler wallet for Firefox.
             For now, a development build of Firefox is required.
             Installation instructions are on the <a href="/wallet">Wallet page</a>.</p>

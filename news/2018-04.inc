          <h3>2018-04: GNU Taler v0.5.0 released</h3>
	  <p>
          We are happy to announce the release of GNU Taler v0.5.0.
          The main new feature is customer tipping, which allows
          merchants to pay small rewards directly into a customer's
          wallet.  Technical improvements include numerous performance
          improvements and bug fixes, as well as a new, simpler to use
          API for merchants that also enables the implementation of
          GNU Taler wallets on platforms that do not support
          WebExtensions.
	  <br>
          The Chrome and Chromium wallets are available for download
	  via the App store.  The exchange, merchant backend and bank
	  components are on the GNU FTP mirrors.
	  </p>

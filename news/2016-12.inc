          <h3>2016-12: Christian Grothoff explains Taler at hasgeek in Bangalore</h3>
          <p>
           <video id="video" poster="{{ url('images/logo-2018-dold.svg')}}" autobuffer="" height="360" width="720" controls="controls">
           <source src="/videos/grothoff2016bangalore.webm" type="video/webm">
           <source src="/videos/grothoff2016bangalore.ogv" type="video/ogv">
           </video>
          </p>
          <p>
          <a rel="license" href="https://creativecommons.org/licenses/by-nd/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="{{ url('images/ccby.png')}}"></a><br>"<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Taler</span>" by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Christian Grothoff, produced by hasgeek</span> is licensed under a <a rel="license" href="https://creativecommons.org/licenses/by-nd/3.0/deed.en_US">Creative Commons Attribution NoDerivatives 3.0 Unported License</a>.
          </p>

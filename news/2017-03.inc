          <h3>2017-03: Sva explains GNU Taler at FOSSASIA in Singapore</h3>
          <p>
           <video id="video" poster="{{ url('images/logo-2018-dold.svg')}}" autobuffer="" height="360" width="640" controls="controls">
           <source src="/videos/taler2017fossasia.webm" type="video/webm">
           <source src="/videos/taler2017fossasia.ogv" type="video/ogv">
           </video>
          </p>
          <p>
          <a rel="license" href="https://creativecommons.org/licenses/by-nd/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="{{ url('images/ccby.png')}}"></a><br>"<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Taler</span>" by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Sva, produced by Engineers.SG</span> is licensed under a <a rel="license" href="https://creativecommons.org/licenses/by-nd/3.0/deed.en_US">Creative Commons Attribution NoDerivatives 3.0 Unported License</a>.
          </p>

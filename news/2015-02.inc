          <h3>2015-02x: Taler becomes an official GNU package</h3>
          <p>Taler was accepted into the GNU project today.  GNU will offer advice,
            advocacy and cooperation --- and host our official public
            mailinglist <a href="mailto:taler@gnu.org">taler@gnu.org</a>.</p>
          <h3>20-07-2017: Sofort&uuml;berweisung unreasonable</h3>
          <p>The German Federal High Court of Justice has decided that
	     the online payment system Sofort&uuml;berweisung is
	     unreasonable, and thus must not be the only payment option offered
	     without additional fees for customers in Germany.
	     <br>
             With GNU Taler, inexpensive instant payments that respect customers
	     privacy will be possible.
          </p>
          <p><a class="btn btn-info" href="https://www.heise.de/newsticker/meldung/BGH-Sofortueberweisung-darf-nicht-einziges-Gratis-Bezahlverfahren-sein-3777580.html" role="button">Source</a></p>

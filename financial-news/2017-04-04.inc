          <h3>04-04-2017: WhatsApp to offer person-to-person payments in India</h3>
          <p>With Indian demonetization policy pushing the country towards
             a cashless society, it was only a question of time until the
             Big Data oligopoly would make its move to take over the country's
             economy.
	     <br>
             With GNU Taler, India would maintain digital sovereignty, and
             the privacy of citizens spending digital cash would
             be preserved while achiving the laudable income transparency and
             anti-counterfeiting goals of the Indian government.
          </p>
          <p><a class="btn btn-info" href="http://mashable.com/2017/04/04/whatsapp-person-to-person-payment-india/#_CZ7RRkKaiqN" role="button">Source</a></p>

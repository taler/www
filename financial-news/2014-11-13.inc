          <h3>13-11-2014: Visa and MasterCard's to move from passwords to biometrics</h3>
          <p>Visa and MasterCard are planning to &quot;simplify hated verification
             systems&quot; by moving from passwords to security codes on mobiles
             and biometrics.  Continuing their flawed insistence on verifying identity,
             Visa and MasterCard will thus build a very personal picture of their
             customers, from shopping habbits down to their cardiac rhythm.<br>
             Taler does not require a customer's identity to verify a payment, as the
             payment system cryptographically verifies the coins.  Thus, Taler does
             not have to intrude into any personal detail of a citizen's life, and
             certainly not their private medical data.
          </p>
          <p><a class="btn btn-info" href="http://www.theguardian.com/money/2014/nov/13/mastercard-visa-kill-off-verification-systems" role="button">Source</a></p>

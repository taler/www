          <h3>30-4-2015: 1970 Researchers Predicted Debit Cards Would be Great for Surveillance</h3>
          <p>&quot;Suppose you were an advisor to the head of the KGB,
          the Soviet Secret Police. Suppose you are given the
          assignment of designing a system for the surveillance of all
          citizens and visitors within the boundaries of the USSR. The
          system is not to be too obtrusive or obvious. What would be
          your decision?&quot;<br>
          The think tank RAND essentially answered this question with
          a blueprint for modern payment systems.  Taler offers an
          escape from the financial panopticon.
          </p>
          <p><a class="btn btn-info" href="http://paleofuture.gizmodo.com/1970s-researchers-predicted-debit-cards-would-be-great-1699216972" role="button">Source</a></p>

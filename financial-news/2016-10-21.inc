          <h3>21-10-2016: Indian banks warn 3.2 million customers</h3>
          <p>A major data breach of Indian banks forced these institutions to warn 3.2 million
	     customers that their accounts might have been compromised and that they need
	     to obtain new bank cards and PIN numbers.<br>
             With GNU Taler, banks can implement privacy by design and minimize data collection,
	     minimizing the impact of security breaches and satisfying GDPR regulations in Europe.
          </p>
          <p><a class="btn btn-info" href="http://www.heise.de/newsticker/meldung/Vermutliche-Datenpanne-Indische-Banken-warnen-3-2-Millionen-Kunden-3356582.html" role="button">Source</a></p>

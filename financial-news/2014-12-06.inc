          <h3>6-12-2014: Visa and MasterCard's uncompetitive business practices</h3>
          <p>The Visa and MasterCard duopoly has eliminated competition among
             banks, setting fees that take away a significant share of profits from
             small merchants.<br>
             Taler is an open standard with free software
             implementations, so merchants do not have to fear a lack of competition.
          </p>
          <p><a class="btn btn-info" href="http://www.ocregister.com/articles/gas-644344-card-fees.html" role="button">Source</a></p>

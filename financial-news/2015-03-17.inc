          <h3>17-3-2015: Pointing Fingers in Apple Pay Fraud</h3>
          <p>Apple Pay may be easy to use, but the simplistic
            user identification creates opportunities for fraud,
            resulting in much higher fraud rates than even with traditional
            credit card systems.<br>
            Taler does not require user identification, enabling
            ease of use while also being effective against fraud.
          </p>
          <p><a class="btn btn-info" href="http://www.nytimes.com/2015/03/17/business/banks-find-fraud-abounds-in-apple-pay.html?_r=0" role="button">Source</a></p>

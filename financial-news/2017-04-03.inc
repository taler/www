          <h3>03-04-2017: Amazon launches Amazon Cash</h3>
          <p>Despite Amazon being in the position of being allowed to keep
             your credit card on file to enable faster payments, they have
             now moved to enable payments without credit cards.
	     <br>
             With GNU Taler, cash-based payments for customers without
             credit cards would be possible not only for big brands that
             can sell tokens at participating retailers, but for all online
             stores as GNU Taler is an open standard and not another
             walled-garden lock-in payment system.
          </p>
          <p><a class="btn btn-info" href="https://techcrunch.com/2017/04/03/amazon-launches-amazon-cash-a-way-to-shop-its-site-without-a-bank-card/" role="button">Source</a></p>

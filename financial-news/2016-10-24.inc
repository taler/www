          <h3>24-10-2016: ApplePay starts in France</h3>
          <p>With ApplePay starting in France, pressure on
	     European banks increase as they are set to
	     lose market share to big technology providers.<br>
             With GNU Taler, we could establish an open standard with a level
	     playing field preserving the independence of national economies
	     by establishing a commons that protects critical infrastructure
	     from domination by a handful of global players.
          </p>
          <p><a class="btn btn-info" href="http://www.heise.de/newsticker/meldung/Apple-Pay-startet-in-Frankreich-3272412.html" role="button">Source</a></p>

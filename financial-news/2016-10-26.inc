          <h3>24-10-2016: Alipay gains acceptance in US and Europe</h3>
          <p>With Alipay being increasingly accepted in retail stores in US and Europe,
	     European banks continue to lose market share to big technology
	     providers.<br>
             With GNU Taler, we could establish an open standard with a level
	     playing field preserving the independence of national economies
	     by establishing a commons that protects critical infrastructure
	     from domination by a handful of global players.
          </p>
          <p><a class="btn btn-info" href="http://www.nfcworld.com/2016/10/24/347979/alipay-gains-acceptance-us-europe/" role="button">Source</a></p>

          <h3>23-07-2016: How banks are refusing to shoulder responsibility for fraud</h3>
          <p>Banks are naturally unhappy about shouldering the cost for fraud, and use
	     various tricks to impose the costs on their customers without providing
	     adequate help to minimize fraud.<br>
             With GNU Taler, cryptography ensures that identity theft and many related
	     types of fraud are no longer possible, allowing banks to offer customers
	     a payment experience where neither side needs to worry about fraud.
          </p>
          <p><a class="btn btn-info" href="http://www.telegraph.co.uk/personal-banking/current-accounts/how-banks-are-refusing-to-shoulder-responsibility-for-fraud/" role="button">Source</a></p>

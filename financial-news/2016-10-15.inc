          <h3>15-10-2016: Thousands of online stores found skimming</h3>
          <p>Security researchers found evidence of adversaries targeting online shops offering credit cards
             to steal and resell credit card credentials.<br>
             With GNU Taler, shops would never receive sensitive personal information such as credit cards,
             thus hacked online shops would not create such hassles for consumers.
          </p>
          <p><a class="btn btn-info" href="https://gwillem.gitlab.io/2016/10/11/5900-online-stores-found-skimming/" role="button">Source</a></p>

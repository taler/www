          <h3>5-12-2014: PayPal for Android gains fingerprint support</h3>
          <p>Following Visa and MasterCard's move to biometrics, PayPal
             now supports authenticating purchases with fingerprint
             recognition.
             Hence, police can now <a href="http://www.findlaw.co.uk/law/government/civil_rights/500374.html">forcefully take user's fingerprints</a> and
             <a href="http://www.wired.com/2013/09/the-unexpected-result-of-fingerprint-authentication-that-you-cant-take-the-fifth/">access their mobile computers</a> and possibly empty their electronic wallets
             <a href="http://www.cbc.ca/news/world/american-shakedown-police-won-t-charge-you-but-they-ll-grab-your-money-1.2760736">in addition to their physical wallets</a>.<br>
             For Taler, we advise users to protect their digital wallets using
             passphrases.
          </p>
          <p><a class="btn btn-info" href="http://www.digitalspy.co.uk/tech/news/a614631/paypal-for-android-gains-fingerprint-support-on-samsung-devices.html" role="button">Source</a></p>

          <h3>5-12-2014: US judge rules banks can sue merchant for bad security</h3>
          <p>Merchants taking credit card data from customers now have to additionally
             fear banks suing them for losses.  It is not suggested that the merchant
             in question was not in compliance with PCI DSS security audit procedures.<br>
             With Taler, merchants never handle sensitive personal credit data, and
             thus neither customers, exchanges nor governments would even have standing to
             sue merchants in court.  Thus, if a merchant system were to be compromised,
             the damage would be limited to the merchant's own operations.
          <p><a class="btn btn-info" href="http://arstechnica.com/tech-policy/2014/12/judge-rules-that-banks-can-sue-target-for-2013-credit-card-hack/" role="button">Source</a></p>

          <h3>15-9-2013: NSA follows the Money</h3>
          <p>Despite the EU allowing the NSA access to financial transaction data to
             track terrorists and organized crime, the NSA saw it necessary to
             target international payment processors including SWIFT and Visa.
             As terrorism and organized crime are covered by legal means, industrial
             espionage to improve the US economy is the only remaining US national
             interest within the NSA's mandate that would explain this illegal activity.<br>
             With Taler, exchanges will only learn the value of a merchant's transactions,
             not who paid or for what (governments may learn what was sold).  Thus,
             the Taler exchange is a significantly less interesting target for industrial
             espionage.
          </p>
          <p><a class="btn btn-info" href="http://www.spiegel.de/international/world/spiegel-exclusive-nsa-spies-on-international-bank-transactions-a-922276.html" role="button">Source</a></p>

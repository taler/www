          <h3>22-12-2015: Sicherheitsforscher hacken das EC-Bezahlsystem</h3>
          <p>Security researchers found serious security flaws in the German &quot;electronic cash&quot; system
             which enable criminals to withdraw funds from merchant accounts based on the information printed
             on receipts and other information obtained from public sources or point-of-sales terminals purchased
             online.<br>
             The German &quot;electronic cash&quot; system is based on the &quot;Poseidon&quot; protocol, for
             which there is no publicly accessible specification or reference implementation. This has allowed
             such major security holes to persist for decades.
          </p>
          <p><a class="btn btn-info" href="http://www.zeit.de/digital/datenschutz/2015-12/electronic-cash-bezahlsystem-terminals-gehackt" role="button">Source</a></p>

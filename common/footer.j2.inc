<footer id="footer">
  <div class="container-fluid cushion-below">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-4">
          <ul class="footer-list">
            <li><p><a href="about.html">{{ _("About") }}</a></p></li>
            <li><p><a href="contact.html">{{ _("Contact") }}</a></p></li>
            <li><p><a href="investors.html">{{ _("For Investors") }}</a></p></li>
          </ul>
        </div>
        <div class="col-12 col-sm-4">
          <ul class="footer-list">
            <li><p><a href="citizens.html">{{ _("Citizens")}} </a></p></li>
            <li><p><a href="merchants.html">{{ _("Merchants") }}</a></p></li>
            <li><p><a href="governments.html">{{ _("Governments") }} </a></p></li>
          </ul>
        </div>
        <div class="col-12 col-sm-4">
          <ul class="footer-list">
            <li><p><a href="developers.html">{{ _("Developer Resources") }}</a></p></li>
            <li><p><a href="faq.html">{{ _("FAQ") }}</a></p></li>
            <li><p><a href="bibliography.html">{{ _("Bibliography") }}</a></p></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="container text-center">
    <p>&copy; <a href="https://gnunet.org/en/ev.html">GNUnet e.V.</a> {{_("and") }}
              <a href="http://inria.fr/">Inria</a> 2015, 2016, 2017</p>
    <p>{{ _("This page was created using <a href='https://www.gnu.org/'>Free Software</a> only.") }}</p>
  </div>
</footer>

<nav class="navbar navbar-toggleable navbar-toggleable-md navbar-expand-md navbar-inverse navbar-dark" style="background-color: black; border-radius: 0px;"> <!-- bg-dark -->
  <div class="container-fluid">
    <button class="navbar-toggler navbar-toggler-right"
            type="button"
            data-toggle="collapse"
            data-target="#navbarMain"
            aria-controls="navbarMain"
            aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarMain">
    <ul class="navbar-nav mr-auto">
      <li class="{{ 'active' if filename == 'index.html' else '' }}"><a class="navbar-brand taler_home" href="index.html">Taler</a></li>
      <li class="nav-item {{ 'active' if filename == 'citizens.html' else '' }}"><a class="nav-link" href="citizens.html">{{ _("Citizens") }} </a></li>
      <li class="nav-item {{ 'active' if filename == 'merchants.html' else '' }}"><a class="nav-link" href="merchants.html">{{ _("Merchants") }} </a></li>
      <li class="nav-item {{ 'active' if filename == 'governments.html' else '' }}"><a class="nav-link" href="governments.html">{{ _("Governments") }}</a></li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ _("Resources") }}</a>
        <ul class="dropdown-menu">
          <li><a class="dropdown-item" href="architecture.html">{{ _("System Architecture") }}</a></li>
          <li><a class="dropdown-item" href="faq.html">{{ _("FAQ") }}</a></li>
          <li><a class="dropdown-item" href="glossary.html">{{ _("Glossary") }}</a></li>
          <li><a class="dropdown-item" href="developers.html">{{ _("Developer Introduction") }} </a></li>
          <li><a class="dropdown-item" href="copyright.html">{{ _("Copyright for Contributors") }} </a></li>
          <li><a class="dropdown-item" href="bibliography.html">{{ _("Bibliography") }}</a></li>
          <li><a class="dropdown-item" href="gsoc-codeless.html">{{ _("Gsoc 2018: Codeless Payments") }}</a></li>
        </ul>
      </li>
    </ul>
    <ul class="navbar-nav navbar-right">
      <li class="nav-item dropdown">
        <button class="btn btn-dark dropdown-toggle"
                type="button"
                id="dropdownMenu1"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="true">
                <img src="{{ url('images/languageicon.svg') }}"
                     height="35"
                     alt="[{{lang}}]" />
                     {{ lang_full }} [{{ lang }}]
        </button>
        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
          <li><a class="dropdown-item" href="{{ self_localized('en') }}">English [en]</a></li>
          <li><a class="dropdown-item" href="{{ self_localized('de') }}">Deutsch [de]</a></li>
          <li><a class="dropdown-item" href="{{ self_localized('fr') }}">Fran&ccedil;ais [fr]</a></li>
          <li><a class="dropdown-item" href="{{ self_localized('it') }}">Italiano [it]</a></li>
          <li><a class="dropdown-item" href="{{ self_localized('es') }}">Espa&ntilde;ol [es]</a></li>
          <li><a class="dropdown-item" href="{{ self_localized('pt') }}">Portugu&ecirc;s [pt]</a></li>
          <li><a class="dropdown-item" href="{{ self_localized('ru') }}">&#x420;&#x443;&#x301;&#x441;&#x441;&#x43A;&#x438;&#x439;&#x20;&#x44F;&#x437;&#x44B;&#x301;&#x43A; [ru]</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
